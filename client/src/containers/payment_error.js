import React from "react";

const PaymentError = props => {
  return (
    <div className="message_container">
      <div style={{ border: "2px solid  #FF395B" }} className="message_box">
        <div className="message_box_left">
          <img
            alt="sad_icon"
            className="image"
            src={
              "https://res.cloudinary.com/marcelakor/image/upload/v1624296911/samples/pizza-web/sad.png"
            }
          />
        </div>
        <div style={{ color: "#FF395B" }} className="message_box_right">
          Payment Error
        </div>
      </div>
    </div>
  );
};

export default PaymentError;
