import React from 'react';
import {NavLink} from 'react-router-dom';
import Logo1 from '../img/logo1.png';

const Navbar = (props)=>{
    return <div className='navbar'>
        <img src={Logo1} className='logo1'/>
        <NavLink exact to={'/'}>Home</NavLink>
        <NavLink exact to={'/menu'}>Menu</NavLink>
        <NavLink exact to={'/customize-your-pizza'}>Customize your pizza</NavLink>
        <NavLink exact to={'/about-us'}>About us</NavLink>
        <NavLink exact to={'/order'}>Order</NavLink>
    </div>
}

export default Navbar;