import React from 'react';
import axios from 'axios';
import { injectStripe } from 'react-stripe-elements';

const Checkout = (props) => {
	console.log('======>', props);
	
	const calculate_total = () => {
		let total = 0;
		props.pizzas.forEach((ele) => (total += ele.quantity * ele.amount));
		return total;
	};
	
	const createCheckoutSession = async () => {
		try {
			const response = await axios.post(
				'http://164.90.162.150/payment/create-checkout-session', //add host
				{ products: props.pizzas }
			);
			debugger;
			return response.data.ok
				? (localStorage.setItem('sessionId', JSON.stringify(response.data.sessionId)),
					redirect(response.data.sessionId))
				: props.history.push('/payment/error');
		} catch (error) {
			props.history.push('/payment/error');
		}
	};

	const redirect = (sessionId) => {
		debugger;
		props.stripe
			.redirectToCheckout({
				// 
				sessionId: sessionId
			})
			.then(function(result) {
				debugger;
				// 
			});
	};

	return (
		<div className="checkout_container">
			<div className="header">
				<h3>Checkout - Check products before pay</h3>
			</div>{' '}
			<br />
				
			<div className="total">
				<b>Total :</b> {calculate_total()} €
			</div>{' '}
			<br />
			<button className="btn btn-primary " onClick={() => createCheckoutSession()}>
				PAY
			</button>{' '}
			<br /> <br />
				
			<div className="products_list">
				{props.pizzas.map((item, idx) => {
					console.log('item =====================>', item);

				//props.ingredients[0].products.map((item, idx)=>{
				//	console.log('item ingredients ===>',item)
				//});

					return (
						<div className="show-1">
							<img src={item.images[0]} /> <br />
							<h3>{item.name}</h3>
								{/*	<h4> {item.ingredients[0].products.name} </h4> <br/> */}
							<div className="show-2"> 
							<button className="btn btn-primary" type="button" onClick={() => props.handleRemove(idx)}>
								Remove
							</button> <br /> <br /> <br />
							</div>
						</div>
					);
				})}
			</div>
			<div className="footer" />
		</div>
	);
};
export default injectStripe(Checkout);
const products = [
	{
		name: 'Carbonara',
		images: [ 'https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/carbonara.jpg' ],
		quantity: 1,
		amount: 10 
	},
];
