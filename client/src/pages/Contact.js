import React, {useState} from 'react';


const Contact = (props) => {
    const [data, setData] = useState({
        name:'',
        email:'',
        subject:'',
        message:'',
    });

    const [error, setError] = useState(''); 
    const [message, setMessage] = useState('');

    const handleInputChange = (e) => {
        setData({
            ...data,
            [e.target.name] : e.target.value
        })
    }
    const sendData = async (e) => {
        e.preventDefault();
        console.log(data.name+' '+ data.email+' '+ data.subject+' '+ data.message)
        if(data.name === '' || data.email === ''|| data.subject === '' || data.message === ''){
            setError('All fieds are requied')
            setMessage(null)
         }else{
            const r = await fetch('http://localhost:4242/emails/send_email',{
                 method:'POST',
                    headers: { 
                     'Content-type': 'application/json'
                   }, 
                   body: JSON.stringify({name: data.name, email: data.email, message:data.message, subject:data.subject})  
               }); 
               const resData = await r.json();
               console.log('resData ===>',resData)
             if (resData.ok){
                alert("Message Sent."); 
                setMessage('Thank you for contacting us!. We have received your message and aur team will contact you as soon as possible.')
           }else{
                alert("Message failed to send.")
          }
            setData({
                name:'',
                email:'',
                subject:'',
                message:'',
            });
            setError(null)
         }
 
    }
    
    return ( <div className='conteiner'>
            <div className='row'>
                <div className='col'>
        <h1>Find Us</h1>
            <p>We are in the center of Barcelona </p> <br/>
            <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d7119.617918758326!2d2.1653607560257564!3d41.38645432411544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x12a4a2f1602b4819%3A0x1eecc2af1c60d64b!2splaza%20catalunya!3m2!1d41.387015399999996!2d2.1700471!5e0!3m2!1ses!2ses!4v1620139986040!5m2!1ses!2ses" 
            width="99%" height="450" allowfullscreen="" loading="lazy" className='location'></iframe> <br/> <br/>
                <div className='info-conteiner'>
                    <ul className='info-contact'>
            <li className='information'> 🍕 <b>Direction:</b> Plaça de Catalunya, 08002 Barcelona, Spain </li>
            <li className='information'> 🍕 <b>Phone namber:</b> 901 905 9XX </li>
            <li className='information'> 🍕 <b>Email:</b> testwebsmar@gmail.com </li>
              </ul>
              </div> <br/>
        <h2>Contact us</h2>
        <p>For any questions leave us a message</p>
                </div>
          </div>
          <div>
                <div>
        <form className='conteiner-form' onSubmit={sendData}>
            <div className='col'>
                <input
                    placeholder='Your name'
                    className='form-control'
                    type='text'
                    name='name'
                    value={data.name}
                    onChange={handleInputChange}
                    autoFocus></input>
            </div>
            <div className='col'>
                <input
                    placeholder='Your email'
                    className='form-control'
                    type='email'
                    value={data.email}
                    name='email'
                    onChange={handleInputChange}
                ></input>
            </div>
            <div className='col'>
                <input
                    placeholder='Subject'
                    className='form-control'
                    type='text'
                    name='subject'
                    value={data.subject}
                    onChange={handleInputChange}
                    ></input>
            </div>
            <div className='col'>
                <textarea
                    placeholder='Write your message'
                    className='form-control'
                    type='textarea'
                    value={data.message}
                    name='message'
                    onChange={handleInputChange}
                ></textarea>
            </div>
            <div className='col'>
                <button className='btn btn-primary' type='submit'>SEND</button> 
            </div>
        </form>
        <div className='error-alert'>{error}</div>
        <div className='message-alert'>{message}</div> 
        </div>
        </div>
        </div>
    );
}
export default Contact;