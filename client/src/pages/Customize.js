import React, {useState} from 'react';
import { Container, Row, Col } from 'reactstrap';
import ingredients from '../static/ingredients';
import Checkout from './Checkout';

const Customize = (props)=>{
    // console.log('props ===>',props)
    const [add, setAdd] = useState([]);
    const [ing, setIng] = useState([]);

    let handleCheckboxChange = (e,amount, name, image, quantity) => {
      if(e.target.checked){
        console.log('amount ====>',amount)
        // console.log('is checked')
        const temp = [...ing];
        temp.push({
          ingredient: e.target.value,
          amount: amount
        })
        setIng(temp)
        // console.log('temp ====>',temp)
      }else{
        const temp = [...ing];
        const index = temp.findIndex( ele => ele.ingredient === e.target.value);
        temp.splice(index, 1);
        setIng(temp)
        // console.log('temp ====>',temp)
      }
    }

    const buttonAddorder =()=>{
     // console.log('Order was adding!')
      // console.log(ing)
      props.addCustomPizza(ing,props.history)
    }; 
    

    let renderIngredients = (obj) => (
        obj.map((item, idx) => {
          // console.log('item =====>',item)
          return <div key={idx}>
              <Row>
          <h4>{obj.category}</h4>
               </Row>
            <Col lg='auto' md='6' sm='12' className='ingredients-list'>
          <input type='checkbox' onChange={(e) => handleCheckboxChange (e,item.amount)} value={item.name}/> {item.name}
          <p>Price: {item.amount}€</p>
          </Col>
          </div>
        })
      )
      
      return <div>
      <h1>You can customize your pizza</h1>
      <p>Create your pizza!!! Choose your favorite ingredients, and enjoy. <br/>
           You can choose all the ingredients your like</p>
           <Container className='customize-container'>
      <h2>Choose the base</h2>
      {
       renderIngredients(ingredients[0].products)
      }
       <h2>Choose the sauce</h2>
      {
       renderIngredients(ingredients[1].products)
     }
      <h2>Choose the type of cheese</h2>
      {
       renderIngredients(ingredients[2].products)
     }
      <h2>Choose the vegetals</h2>
      {
       renderIngredients(ingredients[3].products)
     }
      <h2>Or choose the meats</h2>
      {
       renderIngredients(ingredients[4].products)
     }
     <br/>
      <button onClick={() => buttonAddorder()}className="btn btn-primary">Add Order and Checkout</button> 


        </Container>
    </div>
}
export default Customize;

