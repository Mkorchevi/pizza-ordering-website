import React, {useState} from 'react';
import pizzas from '../static/pizzas';


const Menu = (props)=>{
 const [add, setAdd] = useState([])
  let handleBottonAdd = product => {
    const temp = [...props.pizzas]
   const index = temp.findIndex(ele => ele.name === product.name);
   console.log('index ====>',index)
   if( index === -1 ){
       const prod = {
          name: product.name,
          images: product.images,
          quantity: 1,
          amount: product.amount 
       }
       temp.push(prod)
   }else{
     temp[index].quantity = temp[index].quantity +1
   }
   props.addPizza(temp);
  };
  

   
    let renderPizzas = (arr) => (
        arr.map( (item, idx) => {
          return <div key={idx}>
            <section className='menu'>
                <img className='img-menu' src={item.images[0]} alt="pizza"/>
             <div className='menu-right'>
                 <h3>{item.name}</h3>
                 <p>{item.description}</p>
                <p> <b>Price:</b> {item.amount} €</p> <br/>
                <div className='button-menu'>
                <button onClick={() => handleBottonAdd(item)}className="btn btn-primary">Add   +{props.pizzas.find(ele => ele.name === item.name)?.quantity}</button>
                </div>
            </div>
            </section>
          </div>
        })
      )
    return <div className='control'>
       <h1>All our pizzas are made with a lot of love!!!</h1>
    <span className='menu-container'>
     {
       renderPizzas(pizzas)
     }
    </span>
    <button className="btn btn-primary" onClick={() => {
         props.checkout(props.history)
         } }>checkout</button>
    </div>
}
export default Menu;