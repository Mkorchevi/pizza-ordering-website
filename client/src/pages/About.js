import React from 'react';
import mamma from '../img/mamma.png';

const About = (props)=>{
    return<div className='App'>
        <div className='mamma'>
        <img className='img-mamma' src={mamma} />
        </div>
        <h1>Oh! Mamma artisan pizzas delivery</h1>
        <p className='about-p'> 
            At Oh Mamma we follow the family recipe that has been passed down from generation to generation, 
            to create our delicious pizza base.<br/>
            In addition, all our ingredients are grown in an ecological and sustainable way so as not to lose 
            the essence of our recipe and the charm of the traditional and authentic brought to your door.
        </p>
    </div>
}

export default About;