import React from "react";
import Checkout from "./Checkout";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  console.log(process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Checkout {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
