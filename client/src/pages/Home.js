import slider1 from '../img/slider/slider1.jpg';
import slider2 from '../img/slider/slider2.jpg';
import slider3 from '../img/slider/slider3.jpg';
import React, { useState } from 'react';
import Contact from '../pages/Contact';

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

const items = [
  {
    src: slider1,
    altText: 'imagen 1',
    caption: 'Oh Mamma! Artisan pizzeria delivery',
  },
  {
    src: slider2,
    altText: 'imagen 2',
    caption: 'Select the ingredients and create your pizza',
  },
  {
    src: slider3,
    altText: 'imagen 3',
    caption: 'Receive your pizza at the doors of your house',
  }
];

const Home = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} width='100%'/>
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    <div>
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
      <div className=''>
        <Contact />
      </div>
    </div>
  );
}

export default Home;