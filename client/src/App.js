import React from 'react';
import './App.css';
import './styles/navbar.css';
import './styles/footer.css';
import './styles/home.css';
import './styles/menu.css';
import './styles/customize.css';
import './styles/about.css';
import Home from './pages/Home';
import Menu from './pages/Menu';
import Customize from './pages/Customize';
import About from './pages/About';
import Stripe from './pages/stripe';
import Navbar from './pages/Navbar';
import PaymentSuccess from './containers/payment_success';
import PaymentError from './containers/payment_error';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';


class App extends React.Component {
	state = {
		pizzas: [],
		order: []
	};
	addCustomPizza = (pizza, history) => {
		const customPizza = {
			name: 'custom pizza',
			images: [],
			quantity: 1,
			amount: pizza.reduce((tot, ele) => (tot += ele.amount), 0)
		};
		const temp = [ ...this.state.pizzas ];
		temp.push(customPizza);
		this.setState({ pizzas: temp }, () => {
			console.log('state pizzas ==>', this.state.pizzas);
			history.push('/order');
		});
	};

	addPizza = (pizzas) => {
		this.setState({ pizzas: pizzas }, () => {
			console.log('state pizzas ==>', this.state.pizzas);
		});
	};

	checkout = (history) => {
		// console.log('history ===>',history)
		if (this.state.pizzas.length === 0) {
			return alert('Order is empty');
		}
		const temp = [ ...this.state.pizzas ];
		console.log('state pizzas ==>', this.state.pizzas);
		// temp.push(pizza);
		this.setState({ order: temp }, () => history.push('/order'));
  };
  
  handleRemove = (idx) => {
    // console.log(idx)
    const temp = [ ...this.state.pizzas ];
    temp.splice(idx,1)
    this.setState({ pizzas: temp })
 }

	render() {
		return (
			<div className="App">
				<Router>
					<Navbar />
					<Route exact path="/" render={(props) => <Home getData={this.getData} {...props} />} />
					<Route
						exact
						path="/menu"
						render={(props) => (
							<Menu
								{...props}
								product={this.state.product}
								addPizza={this.addPizza}
								pizzas={this.state.pizzas}
								checkout={this.checkout}
							/>
						)}
					/>
					<Route
						exact
						path="/customize-your-pizza"
						render={(props) => <Customize {...props} addCustomPizza={this.addCustomPizza} />}
					/>
					<Route exact path="/about-us" component={About} />
					<Route
						exact
						path="/order"
						render={(props) => <Stripe {...props} pizzas={this.state.pizzas} handleRemove={this.handleRemove}/>}
					/>
					<Route exact path="/payment/success" render={(props) => <PaymentSuccess {...props} />} />
					<Route exact path="/payment/error" render={(props) => <PaymentError {...props} />} />
				</Router>
				<footer className="footer">
					<div className="footer-text">
						<p>
							Oh! Mamma artisan Pizzas delivery <br />
							Contact 901 905 9xx | Email: testwebsmar@gmail.com
						</p>
					</div>
				</footer>
			</div>
		);
	}
}
export default App;
