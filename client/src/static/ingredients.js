let ingredients =[
    {
        category: 'base',
        products: [
            {
                name:'Fine',
                amount: 6,
                images:[],
                description:'',
                quantity: 0,
            },
            {
                name:'Normal',
                amount: 7,
                images:[],
                description:'',
                quantity: 0,
            },
        ]
    },
    {
        category:'souce',
        products:[
            {
                name:'BBQ souce',
                amount: 1,
                images:[],
                description:'',
                quantity: 0,
            },
            {
                name:'Tomato souce',
                amount: 0.5,
                images:[],
                description:'',
                quantity: 0,
            },
            {
                name:'Carbonara Cream',
                amount: 1,
                images:[],
                description:'',
                quantity: 0,
            },
            {
                name:'Barbecue Texas sauce',
                amount: 1,
                images:[],
                description:'',
                quantity: 0,
            },
        ]
    }, 
    {
            category:'cheese',
            products: [
                {
                    name: 'Mozzarella',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Goat Cheese',
                    amount: 1.5,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Emmental Cheese',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Cheddar',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Parmesan',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
            ]
        },
        {
            category:'vegetal',
            products: [
                {
                    name: 'Tomato',
                    amount: 0.5,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Onion',
                    amount: 0.5,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Caramelized Onion',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Pepper',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Mushrooms',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name: 'Olive',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
            ]
        },
        {
            category: 'meats',
            products: [
                {
                    name:'Minced Beef',
                    amount: 3,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name:'Chicken',
                    amount: 2,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name:'Bacon',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name:'Pepperoni',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
                {
                    name:'Ham',
                    amount: 1,
                    images:[],
                    description:'',
                    quantity: 0,
                },
            ]
        },
]

export default ingredients;