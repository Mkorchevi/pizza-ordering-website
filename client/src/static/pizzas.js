//import pizza1 from '../img/pizzas/pizza1.jpg';

let pizzas = [
    {
        name     :'Carbonara',
        amount   :11.90,
        images   : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/carbonara.jpg"],
        description : 'MOZZARELLA, ONION, BACON, MUSHROOMS AND CARBONARA CREAM.',
        quantity     : 0,
    },
    {
        name     :'Mediterránea',
        amount       : 10.90,
        images      : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/mediterr%C3%A1nea.jpg"],
        description : 'TOMATO SAUCE, MOZZARELLA, TUNA, ONION',
        quantity     : 0,
    },
    {
        name     :'Toscana',
        amount       : 14.90,
        images       : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/toscana.jpg"],
        description : 'TOMATO, MOZZARELLA, DRIED TOMATO, BURRATA, OLIVE OIL, BASIL OIL, ARUGULA.',
        quantity     : 0,
    },
    {
        name     :'Pepperoni',
        amount       : 11.90,
        images       : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/pepperoni.jpg"],
        description : 'TOMATO SAUCE, MOZZARELLA, PEPPERONI.',
        quantity    : 0,
    },
    {
        name     :'BBQ',
        amount       : 12.90,
        images      : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654926/samples/pizza-web/bbq.jpg"],
        description : 'TOMATO SAUCE, MOZZARELLA, SLOW-COOKED BEEF, CHICKEN, ONION, BACON, BARBECUE SAUCE',
        quantity     : 0,
    },
    {
        name     :'Vegetarian',
        amount       : 12.90,
        images       : ["https://res.cloudinary.com/marcelakor/image/upload/v1620654927/samples/pizza-web/vegetarian.jpg"],
        description : 'TOMATO, VEGAN MOZZARELLA, FIG ONION, MUSHROOMS, ZUCCHINI, DRIED TOMATO AND BASIL OIL.',
        quantity     : 0,
    },
]
export default pizzas;