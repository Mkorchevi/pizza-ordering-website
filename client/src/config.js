import axios from 'axios';
// =======  preparing to the deplyment  ========
const URL = window.location.hostname === `localhost`
            ? `http://localhost:4242` // 4242 should be replaced with your server port
            : `http://164.90.162.150` // it should be replaced with actual domain during the deployment
// =============================================
const customInstance = axios.create ({
  baseURL : URL, 
  headers: {'Accept': 'application/json'}
})

export default customInstance;