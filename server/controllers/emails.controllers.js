const nodemailer = require('nodemailer')
// 
const transport = nodemailer.createTransport({
// 
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD,
	}

});
const send_email = async (req,res) => {
	  const { name , email, subject , message } = req.body
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
	  	// 
		    to: process.env.NODEMAILER_EMAIL,
		    subject: "New message from " + name,
		    html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + message +'</pre> <pre>' + email +'</pre></p>'
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({ok:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_email }