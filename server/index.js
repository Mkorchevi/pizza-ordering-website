const express = require("express");
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const cors = require("cors");
app.use(cors());

require("dotenv").config({ path: "./.env" });

app.use("/payment", require("./routes/payment.route.js"));
app.use('/emails', require('./routes/emails.routes.js'));


const path = require('path');

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

var PORT = process.env.PORT || 4242
app.listen(PORT, () =>
  console.log(`🚀  Node server listening on port ${PORT}!  🚀`)
);
